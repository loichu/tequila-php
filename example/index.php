<?php
require __DIR__ . '/vendor/autoload.php';
use EPFL\Tequila\TequilaClient;

$oClient = new TequilaClient();

// Application name
$oClient->SetApplicationName('Test tequila auth');

// Wanted attributes
$oClient->SetWantedAttributes(array('uniqueid','name','firstname','unit', 'unitid', 'where', 'group'));
$oClient->SetWishedAttributes(array('email', 'title'));

// Application and server
$oClient->SetApplicationURL('http://localhost:8080');
#$oClient->SetServer("https://tequila.epfl.ch/cgi-bin/tequila");
#$oClient->SetServer("http://localhost:8080");
#$oClient->SetServerURL("https://tequila.epfl.ch/cgi-bin/tequila");
#$oClient->SetServerURL("http://localhost:8080");
$oClient->SetCookieName = "TequilaCookie";

// Launch authentication
$oClient->Authenticate ();

// Get attributes
$org  = $oClient->getValue('org');
$user = $oClient->getValue('user');
$host = $oClient->getValue('host');
$sKey = $oClient->GetKey();






// Display
echo <<<EOT
<html lang="en">
	<head>
		<title>Test Tequila</title>
	</head>
	<body>
		<h3>Test Tequila :</h3>
		<pre>
             key = $sKey
             org = $org
            user = $user
            host = $host

EOT;

echo "\nCookies = ";
print_r($_COOKIE);
echo "\nSession = ";
print_r($_SESSION);

echo <<<EOT
		</pre>
		<p>
		<a href="{$_SERVER['PHP_SELF']}">Test session key</a><br/>
	</body>
</html>

EOT;
?>
